db.fruits.insertMany([
    {
        "name": "Banana",
        "supplier": "Farmer Fruits Inc.",
        "stocks": 30,
        "price": 20,
        "onSale": true
    },
    {
        "name": "Mango",
        "supplier": "Mango Magic Inc.",
        "stocks": 50,
        "price": 70,
        "onSale": true
    },
    {
        "name": "Dragon Fruit",
        "supplier": "Farmer Fruits Inc.",
        "stocks": 10,
        "price": 60,
        "onSale": true
    },
    {
        "name": "Grapes",
        "supplier": "Fruity Co.",
        "stocks": 30,
        "price": 100,
        "onSale": true
    },
    {
        "name": "Apple",
        "supplier": "Apple Valley",
        "stocks": 0,
        "price": 20,
        "onSale": false
    },
    {
        "name": "Papaya",
        "supplier": "Fruity Co.",
        "stocks": 15,
        "price": 60,
        "onSale": true
    }
]);

//2.Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
        {
            $match: {
                "onSale": true
            }
        },
        {
            $count: "fruitsOnSale"
        }
    ]);





//3. Use the count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
        {
            $match: {
                "stocks": { $gt: 20 }
            }
        },
        {
            $count: "enoughStock"
        }
    ]);




//4. Use the average operator to get the average price of fruits onSale per supplier.


db.fruits.aggregate([
    {
        $match: {
            "onSale": true
        }
    },
    {
        $group: {
            _id: "$supplier", average_price: {$avg: "$price"}
        }
    }
]);


//5. Use the max operator to get the highest price of a fruit per supplier.



db.fruits.aggregate([
    {
        $match: {
            "onSale": true
        }
    },
    {
        $group: {
            _id: "$supplier", max_price: {$max: "$price"}
        }
    }
]);






///6 Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
     {
        $match: {
            "onSale": true
        }
    },
    {
        $group: {
            _id: "$supplier", min_price: {$min: "$price"}
        }
    }
]);
